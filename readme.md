# Teste Dev Uplexis - Consulta Sintegra Espirito Santo

## - Instalação

- Clonar o repositório que esta no Bitbucket
```sh
$ git clone https://ph_couto@bitbucket.org/ph_couto/teste-dev-uplexis.git
```
- Atualizar com Composer
```sh
$ composer update
```
- Criar um schema no MySQL
- Altere o arquivo .env com as informações do seu banco de dados

- Rodar as migrations para criar as tabelas no banco
```sh
$ php artisan migrate
```
- Rodar um servidor de desenvolvimento em: ```http://localhost:8000```
```sh
$ php artisan serve
```
