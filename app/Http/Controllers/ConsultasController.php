<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sintegra;

class ConsultasController extends Controller
{
    public function index() {
        $consultas = Sintegra::all();
        return view('consultas', ['consultas' => $consultas]);
    }
    
    public function abre_registro($id) {
        $registro = Sintegra::find($id);
        return view('registro', ['registro' => $registro]);
    }
    
    public function apaga_registro($id) {
        $apaga = Sintegra::find($id);
        $apaga->delete();
        return redirect('/consultas');
    }
}
