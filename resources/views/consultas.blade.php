@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                
                <div class="panel-heading">Consultar CNPJ - Sintegra: Espírito Santo</div>

                <div class="panel-body">
                    
                    <p>Clique no registro para ver informações</p>

                    <ul class="list-group">
                        @foreach ($consultas as $consulta)
                        <li class="list-group-item">
                            <a href="/consultas/{{ $consulta->id }}"><b>{{ $consulta->created_at }}</b> - CNPJ: {{ $consulta->cnpj }}</a>
                         </li>

      
   
                        @endforeach
                    </ul>
                    
                    <a class="btn btn-primary" href="{{ url('/home') }}">Voltar</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
